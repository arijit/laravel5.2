<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    function index(){
        return view('admin.site.index');
    }
    function getLogin(){
        return view('admin.site.login');
    }
}
