<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Auth;

class SiteController extends Controller {

    function index() {
        return view('frontend.site.index');
    }

    function login() {
        return view('frontend.site.login');
    }

    function getLogin(Request $request) {
        $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                    'password' => 'required',
        ]);
        $messages = $validator->errors();
        if ($validator->fails()) {
            return redirect('login')
                            ->withErrors($validator)
                            ->withInput();
        }

        $email = $_POST['email'];
        $password = $_POST['password'];
        if (Auth::guard('frontend')->attempt(['email' => $email, 'password' => $password])) {

            return redirect('/dashboard');
        } else {
            $validator->errors()->add('invalid_login', 'invalid login credentials!');
             return redirect('login')
                            ->withErrors($validator)
                            ->withInput();
        }
    }

    function dashboard() {
        print_r(Auth::user()->id);
        exit();
    }

    function logout() {
        Auth::guard('frontend')->logout();
        return redirect('/site');
    }

}
