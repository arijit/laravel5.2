<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

//Route::get('/', function () {
//    return view('welcome');
//});
//Route::get('create', 'SiteController@create');
//Route::post('signin', 'SiteController@signin');
//Route::get('update/{id}', 'SiteController@update');
//Route::auth();
//
//Route::get('/home', 'HomeController@index');

Route::group(['namespace' => 'frontend'], function() {
    Route::get('site', 'SiteController@index');
    Route::get('/', 'SiteController@index');
});
Route::group(['namespace' => 'frontend','middleware'=>'frontend_not_loggedin'], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'SiteController@login']);
    Route::post('login', ['as' => 'login', 'uses' => 'SiteController@getLogin']);
});
Route::group(['namespace' => 'frontend','middleware'=>'frontend_loggedin'], function() {
    Route::get('logout', 'SiteController@logout');
    Route::get('dashboard', 'SiteController@dashboard');
});


Route::group(['namespace' => 'admin','prefix' => 'admin','middleware'=>'admin_not_loggedin'], function() {
    Route::get('', ['as' => 'login', 'uses' => 'SiteController@getLogin']);
    Route::get('/', ['as' => 'login', 'uses' => 'SiteController@getLogin']);
    Route::get('login', ['as' => 'login', 'uses' => 'SiteController@getLogin']);
});
Route::group(['namespace' => 'admin','prefix' => 'admin','middleware'=>'admin_loggedin'], function() {
//Route::group(['namespace' => 'admin','prefix' => 'admin'], function() {
    Route::get('site', 'SiteController@index');
});

