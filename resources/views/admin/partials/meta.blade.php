<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>
     {{Html::style('assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}
     {{Html::style('assets/admin/bower_components/metisMenu/dist/metisMenu.min.css')}}
     {{Html::style('assets/admin/dist/css/timeline.css')}}
     {{Html::style('assets/admin/dist/css/sb-admin-2.css')}}
     {{Html::style('assets/admin/bower_components/morrisjs/morris.css')}}
     {{Html::style('assets/admin/bower_components/font-awesome/css/font-awesome.min.css')}}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->