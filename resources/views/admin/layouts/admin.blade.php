<!DOCTYPE html>
<html lang="en">

<head>
    @include('admin.partials.meta')
</head>

<body>

    <div id="wrapper">
        @include('admin.partials.header')
        <div id="page-wrapper">
             @yield('content')
        </div>
        <!-- /#page-wrapper -->
    </div>
    @include('admin.partials.footer')
</body>
</html>
