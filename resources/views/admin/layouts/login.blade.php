<!DOCTYPE html>
<html lang="en">

    <head>
        @include('admin.partials.meta')
    </head>

    <body>
        @yield('content') 
        @include('admin.partials.footer')
    </body>
</html>
