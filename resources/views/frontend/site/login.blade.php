@extends('frontend.layouts.frontend')
@section('content')
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="login-form"><!--login form-->
                    <h2>Login to your account</h2>
                    <form method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        @if($errors->has('invalid_login')) 
                        <div class="alert alert-danger">{{ $errors->first('invalid_login') }}</div>
                        @endif
                        {{ Form::text('email', null,array('placeholder'=>'email',$attributes = $errors->has('email') ? array('class'=>'error') : array())) }}, 
                       <span>{{ $errors->first('email') }}</span>
                        <div>
                            <input type="password" placeholder="Password" name="password" value="{{ old('password') }}"/>
                            <span>{{ $errors->first('password') }}</span>
                        </div>
                        <span>
                            <input type="checkbox" class="checkbox"> 
                            Keep me signed in
                        </span>
                        <button type="submit" class="btn btn-default">Login</button>
                    </form>
                </div><!--/login form-->
            </div>
            <div class="col-sm-1">
                <h2 class="or">OR</h2>
            </div>
            <div class="col-sm-4">
                <div class="signup-form"><!--sign up form-->
                    <h2>New User Signup!</h2>
                    <form action="#">
                        <input type="text" placeholder="Name"/>
                        <input type="email" placeholder="Email Address"/>
                        <input type="password" placeholder="Password"/>
                        <button type="submit" class="btn btn-default">Signup</button>
                    </form>
                </div><!--/sign up form-->
            </div>
        </div>
    </div>
</section><!--/form-->
@endsection
