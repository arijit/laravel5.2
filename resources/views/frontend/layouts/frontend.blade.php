<!DOCTYPE html>
<html lang="en">
<head>
      @include('frontend.partials.meta')
</head><!--/head-->

<body>
      @include('frontend.partials.header')
	<!--/header-->
	
	   @yield('content')
	
	@include('frontend.partials.footer')
</body>
</html>